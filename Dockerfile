FROM mhart/alpine-node:13.8.0
# Create app directory
WORKDIR /usr/src/app

COPY src/index.js .

EXPOSE 5000

CMD [ "node", "index" ]